package org.videolan.vlcbenchmark.benchmark

import android.media.projection.MediaProjectionManager
import androidx.lifecycle.ViewModel
import org.videolan.vlcbenchmark.results.ResultController
import org.videolan.vlcbenchmark.tests.TestController

class BenchmarkViewModel : ViewModel() {

    companion object {
        @Suppress("UNUSED")
        private val TAG = this::class.java.name
    }

    lateinit var testController: TestController
    lateinit var resultController: ResultController

    var running: Boolean = false
    var projectionManager: MediaProjectionManager? = null
    var orientation: Int = -1

}