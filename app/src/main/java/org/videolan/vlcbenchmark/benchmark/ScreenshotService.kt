package org.videolan.vlcbenchmark.benchmark

import android.annotation.TargetApi
import android.app.*
import android.content.*
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.PixelFormat
import android.hardware.display.VirtualDisplay
import android.media.Image
import android.media.ImageReader
import android.media.projection.MediaProjection
import android.os.Binder
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.util.Log
import org.videolan.vlcbenchmark.Constants
import org.videolan.vlcbenchmark.R
import org.videolan.vlcbenchmark.VLCWorkerModel
import org.videolan.vlcbenchmark.tools.StorageManager.tmpScreenshotDir
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class ScreenshotService() : Service() {

    private var virtualDisplay: VirtualDisplay? = null
    private var imageReader: ImageReader? = null
    private var handler: Handler = Handler()
    private var screenshotNumber: Int = 0
    private var mediaProjection: MediaProjection? = null
    private var binder: Binder = ScreenshotServiceBinder()

    private var width: Int = 0
    private var height: Int = 0
    private var density: Int = 0
    private var hasSetup = false

    private var br: BroadcastReceiver = ScreenshotBroadcastReceiver()

    override fun onCreate() {
        val filter = IntentFilter(Constants.ACTION_TRIGGER_SCREENSHOT)
        registerReceiver(br, filter)
        val notification = createNotification()
        startForeground(1, notification)
        super.onCreate()
    }

    inner class ScreenshotBroadcastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent != null) {
                if (intent.action == Constants.ACTION_TRIGGER_SCREENSHOT) {
                    if (intent.hasExtra("screenshot")) {
                        screenshotNumber = intent.getIntExtra("screenshot", 0)
                        prepareScreenshot()
                    }
                }
            }
        }
    }

    private fun createNotification(): Notification {
        val notificationChannelId = "VLCBENCHMARK SERVICE CHANNEL"

        // depending on the Android API that we're dealing with we will have
        // to use a specific method to create the notification
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager;
            val channel = NotificationChannel(
                    notificationChannelId,
                    "VLCBenchmark notifications channel",
                    NotificationManager.IMPORTANCE_HIGH
            ).let {
                it.description = "VLCBenchmark Service channel"
                it
            }
            notificationManager.createNotificationChannel(channel)
        }

        val pendingIntent: PendingIntent = Intent(this, VLCWorkerModel::class.java).let { notificationIntent ->
            PendingIntent.getActivity(this, 0, notificationIntent, 0)
        }

        val builder: Notification.Builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) Notification.Builder(
                this,
                notificationChannelId
        ) else Notification.Builder(this)

        return builder
                .setContentTitle("VLCBenchmark Screenshot Service")
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker("VLCBenchmark Screenshot Service")
                .setPriority(Notification.PRIORITY_HIGH) // for under android 26 compatibility
                .build()
    }

    override fun onBind(p0: Intent?): IBinder? {
        return binder
    }

    override fun onDestroy() {
        unregisterReceiver(br)
        super.onDestroy()
    }

    fun setup(width: Int, height: Int, density: Int, mediaProjection: MediaProjection) {
        this.width = width
        this.height = height
        this.density = density
        this.mediaProjection = mediaProjection
        hasSetup = true
    }

    fun prepareScreenshot() {
        imageReader = ImageReader.newInstance(width, height, PixelFormat.RGBA_8888, 2)
        virtualDisplay = mediaProjection?.createVirtualDisplay("testScreenshot", width,
                height, density, VIRTUAL_DISPLAY_FLAGS,
                imageReader!!.surface, null, handler)

        if (virtualDisplay == null) {
            Log.e(TAG, "prepareScreenshot: Failed to create Virtual Display")
        }
        try {
            imageReader!!.setOnImageAvailableListener(ImageAvailableListener(), handler)
        } catch (e: IllegalArgumentException) {
            Log.e(TAG, "prepareScreenshot: Failed to create screenshot callback")
        }
    }

    inner class ScreenshotServiceBinder : Binder() {
        fun getService() : ScreenshotService = this@ScreenshotService
    }

    fun continueVlcAndroid() {
        val broadcastIntent = Intent(Constants.ACTION_CONTINUE_BENCHMARK)
        broadcastIntent.setPackage(getString(R.string.vlc_package_name))
        sendBroadcast(broadcastIntent)
    }

    /**
     * Callback that is called when the first image is available after setting up
     * ImageReader in onEventReceive(...) at the end of the video buffering.
     *
     * It takes the screenshot.
     */
    @TargetApi(19)
    private inner class ImageAvailableListener : ImageReader.OnImageAvailableListener {
        @TargetApi(21)
        override fun onImageAvailable(reader: ImageReader) {
            handler.postDelayed({
                var outputStream: FileOutputStream? = null
                var image: Image? = null
                val bitmap: Bitmap?
                try {
                    image = imageReader!!.acquireLatestImage()
                } catch (e: IllegalArgumentException) {
                    Log.e(TAG, "Failed to acquire latest image for screenshot.")
                }

                if (image != null) {
                    val planes = image.planes
                    val buffer = planes[0].buffer.rewind()
                    val pixelStride = planes[0].pixelStride
                    val rowStride = planes[0].rowStride
                    val rowPadding = rowStride - pixelStride * width

                    bitmap = Bitmap.createBitmap(width + rowPadding / pixelStride, height,
                            Bitmap.Config.ARGB_8888)
                    if (bitmap != null) {
                        bitmap.copyPixelsFromBuffer(buffer)
                        val folder = File(tmpScreenshotDir)

                        if (!folder.exists()) {
                            if (!folder.mkdir()) {
                                Log.e(TAG, "onImageAvailable: Failed to create screenshot directory")
                                return@postDelayed
                            }
                        }
                        val imageFile = File(folder.absolutePath + File.separator + "Screenshot_" + screenshotNumber + ".png")
                        try {
                            outputStream = FileOutputStream(imageFile)
                        } catch (e: IOException) {
                            Log.e(TAG, "Failed to create outputStream")
                        }

                        if (outputStream != null) {
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
                        }

                        bitmap.recycle()
                        image.close()
                        if (outputStream != null) {
                            try {
                                outputStream.flush()
                                outputStream.close()
                            } catch (e: IOException) {
                                Log.e(TAG, "Failed to release outputStream")
                            }

                        }
                    }
                }
                try {
                    imageReader!!.setOnImageAvailableListener(null, null)
                } catch (e: IllegalArgumentException) {
                    Log.e(TAG, "Failed to delete ImageReader callback")
                }

                virtualDisplay!!.release()
                virtualDisplay = null
                imageReader!!.close()

                continueVlcAndroid()

            }, 3000)
        }
    }

    companion object {
        private const val VIRTUAL_DISPLAY_FLAGS = 0
        @Suppress("UNUSED")
        private val TAG = this::class.java.name
    }
}