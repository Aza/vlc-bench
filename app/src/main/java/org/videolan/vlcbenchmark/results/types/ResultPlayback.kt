package org.videolan.vlcbenchmark.results.types

import kotlinx.android.parcel.Parcelize
import org.json.JSONObject
import org.videolan.vlcbenchmark.Constants
import org.videolan.vlcbenchmark.results.ResultModel

@Parcelize
class ResultPlayback(
        override var name: String = "",
        override var hardware: Boolean = false,
        override var score: Double = 30.0,
        override var maxScore: Double = 30.0,
        override var type: Constants.ResultType = Constants.ResultType.PLAYBACK,
        override var crash: String = "",
        override var stacktrace: String = "",
        var warnings: Int = 0,
        var framesDropped: Int = 0
) : ResultModel (name, hardware, score, maxScore, type, crash, stacktrace) {

    constructor(jsonObject: JSONObject) : this() {
        this.name = jsonObject.getString("name")
        this.score = jsonObject.getInt("score").toDouble()
        this.hardware = jsonObject.getBoolean("hardware")
        this.crash = jsonObject.getString("crash")
        this.stacktrace = jsonObject.getString("stacktrace")
        this.warnings = jsonObject.getInt("warnings")
        this.framesDropped = jsonObject.getInt("frames_dropped")
    }

    override fun jsonDump() : JSONObject {
        val jsonObject = super.jsonDump()
        jsonObject.put("warnings", warnings)
        jsonObject.put("frames_dropped", framesDropped)
        jsonObject.put("max_score", this.maxScore)
        return jsonObject
    }

    fun setResults(warnings: Int, framesDropped: Int) {
        this.warnings = warnings
        this.framesDropped = framesDropped
        this.score -= warnings
        this.score -= 2 * framesDropped
        if (this.score < 0)
            this.score = 0.0
    }

    companion object {
        @Suppress("UNUSED")
        private val TAG = this::class.java.name
    }
}