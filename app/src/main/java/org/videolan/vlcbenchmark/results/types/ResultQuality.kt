package org.videolan.vlcbenchmark.results.types

import kotlinx.android.parcel.Parcelize
import org.json.JSONArray
import org.json.JSONObject
import org.videolan.vlcbenchmark.Constants
import org.videolan.vlcbenchmark.results.ResultModel
import org.videolan.vlcbenchmark.tests.types.TestQuality
import org.videolan.vlcbenchmark.benchmark.ScreenshotValidator
import org.videolan.vlcbenchmark.tools.StorageManager
import org.videolan.vlcbenchmark.tools.StorageManager.deleteDirectory
import org.videolan.vlcbenchmark.tools.StorageManager.getInternalDirStr
import org.videolan.vlcbenchmark.tools.StorageManager.tmpScreenshotDir
import java.io.File

@Parcelize
class ResultQuality(
        override var name: String = "",
        override var hardware: Boolean = false,
        override var score: Double = 20.0,
        override var maxScore: Double = 20.0,
        override var type: Constants.ResultType = Constants.ResultType.QUALITY,
        override var crash: String = "",
        override var stacktrace: String = "",
        var percentOfBadScreenshot: Double = 0.0,
        var screenshotNames: ArrayList<String> = ArrayList(),
        var screenshotScores: ArrayList<Int> = ArrayList()
) : ResultModel(name, hardware, score, maxScore, type, crash, stacktrace) {

    constructor(jsonObject: JSONObject) : this() {
        this.name = jsonObject.getString("name")
        this.score = jsonObject.getInt("score").toDouble()
        this.hardware = jsonObject.getBoolean("hardware")
        this.crash = jsonObject.getString("crash")
        this.stacktrace = jsonObject.getString("stacktrace")
        this.percentOfBadScreenshot = jsonObject.getInt("percent_of_bad_screenshots").toDouble()
        val screenshots = jsonObject.getJSONArray("screenshots")
        for (i in 0 until screenshots.length()) {
            this.screenshotNames.add(screenshots[i].toString())
        }
        val screenshotsScores = jsonObject.getJSONArray("screenshot_scores")
        for (i in 0 until screenshotsScores.length()) {
            this.screenshotScores.add(screenshotsScores[i].toString().toInt())
        }
    }

    override fun jsonDump(): JSONObject {
        val jsonObject = super.jsonDump()
        jsonObject.put("percent_of_bad_screenshots", percentOfBadScreenshot.toInt())
        jsonObject.put("screenshots", JSONArray() )
        val screenshotScoresJSONArray = JSONArray(screenshotScores)
        jsonObject.put("screenshot_scores", screenshotScoresJSONArray)
        jsonObject.put("max_score", this.maxScore)
        return jsonObject
    }

    fun jsonDumpWithScreenshots(): JSONObject {
        val jsonObject = jsonDump()
        val screenshotJSONArray = JSONArray(screenshotNames)
        jsonObject.put("screenshots", screenshotJSONArray)
        val screenshotScoresJSONArray = JSONArray(screenshotScores)
        jsonObject.put("screenshot_scores", screenshotScoresJSONArray)
        return jsonObject
    }

    fun setResults(test: TestQuality, testIndex: Int) {
        val tmpScreenshotFolder = tmpScreenshotDir
        val screenshotDir = getInternalDirStr(StorageManager.screenshotFolder)
        val numberOfScreenshot: Int = test.timestamps.size
        val colors = test.colors

        var badScreenshots = 0
        for (i in 0 until numberOfScreenshot) {
            val filePath = "$tmpScreenshotFolder/$SCREENSHOT_NAMING$i.png"
            val file = File(filePath)
            val res = ScreenshotValidator.validateScreenshot(filePath, colors[i])
            if (res != null && !res.first!!) {
                badScreenshots++
                var fileName: String = (testIndex + 1)
                        .toString() + "_" + test.getTypeString()
                fileName += "_$i.png"
                val renamedFile = File("$screenshotDir/$fileName")
                if (!file.renameTo(renamedFile)) {
                    fileName = ""
                }
                this.screenshotNames.add(fileName)
                this.screenshotScores.add(res.second!!)
            }
        }
        this.percentOfBadScreenshot = badScreenshots.toDouble() / numberOfScreenshot * 100
        this.score -= badScreenshots.toDouble() / numberOfScreenshot * maxScore
        deleteDirectory(tmpScreenshotDir)
    }

    companion object {
        @Suppress("UNUSED")
        private val TAG = this::class.java.name
        private const val SCREENSHOT_NAMING = "Screenshot_"
    }
}