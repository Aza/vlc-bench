package org.videolan.vlcbenchmark.results

import android.os.Parcelable
import org.json.JSONObject

interface IResult : Parcelable {
    fun jsonDump() : JSONObject
    fun setFailure(crash: String, stacktrace: String)
}