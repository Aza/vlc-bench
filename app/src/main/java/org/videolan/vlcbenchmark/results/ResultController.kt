package org.videolan.vlcbenchmark.results

import android.content.Context
import android.os.Parcelable
import android.util.Log
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue
import org.videolan.vlcbenchmark.Constants
import org.videolan.vlcbenchmark.results.types.ResultPlayback
import org.videolan.vlcbenchmark.results.types.ResultQuality
import org.videolan.vlcbenchmark.results.types.ResultSpeed

@Parcelize
class ResultController(
        var resultArray: @RawValue Array<ArrayList<ResultModel>>? = null,
        var currentLoopIndex: Int = 0,
        var maxLoop: Int = 0
) : Parcelable {

    fun getCurrentTestIndex() : Int {

        return if (resultArray != null) {
            for (res in resultArray?.get(0)!!) {
                Log.d(TAG, "getCurrentTestIndex: ${res.name}")
            }
            resultArray!![currentLoopIndex].size
        } else {
            Log.d(TAG, "getCurrentTestIndex: returning 0")
            0
        }
    }

    fun reset(context: Context, numberOfLoops: Int) {
        resultArray = Array(numberOfLoops) { ArrayList<ResultModel>() }
        saveResults(context)
        maxLoop = numberOfLoops
    }

    fun setResultList(context: Context, resultArray: Array<ArrayList<ResultModel>>, numberOfLoops: Int) : Boolean {
        if (numberOfLoops != resultArray.size) {
            ResultRepository().discardCurrentResultList(context)
            this.resultArray = Array(numberOfLoops) { ArrayList<ResultModel>() }
            return false
        } else if (resultArray.isEmpty()) {
            return false
        }
        maxLoop = numberOfLoops
        var loopIndex = 0
        // finding the benchmark loop
        if (numberOfLoops == 3) {
            while (loopIndex < numberOfLoops) {
                if (resultArray[loopIndex].size == 0) break
                loopIndex += 1
            }
            currentLoopIndex = if (loopIndex == 0) 0 else loopIndex - 1
        }
        this.resultArray = resultArray
        return true
    }

    private fun saveResults(context: Context) {
        if (resultArray != null) {
            ResultRepository().setCurrentResultList(context, resultArray!!)
        }
    }

    fun addResults(context: Context, result: ResultModel) {
        if (resultArray != null) {
            ResultRepository().addToCurrentResultList(context, result, currentLoopIndex)
            // Clearing stacktrace so that it is stored in json, but not in app memory
            result.clearStacktrace()
            resultArray!![currentLoopIndex].add(result)
        } else {
            Log.e(TAG, "addResults: resultArray is null")
        }
    }

    fun clearStacktraces() {
        for (i in resultArray!!.indices) {
            for (j in resultArray!![i].indices) {
                resultArray!![i][j].clearStacktrace()
            }
        }
    }

    companion object {
        @Suppress("UNUSED")
        private val TAG = this::class.java.name

        fun mergeResults(results: Array<ArrayList<ResultModel>>): ArrayList<ResultModel>? {
            val mergedResults = ArrayList<ResultModel>()
            for (i in 0 until results[0].size) {
                val res = results[0][i]
                for (j in 1 until results.size) {
                    val toComp = results[j][i]
                    if (res.name != toComp.name || res.type != toComp.type || res.hardware != toComp.hardware) {
                        Log.e(TAG, "mergeResults: Difference in ResultModels")
                        return null
                    }
                    res.score += toComp.score
                    when (res.type) {
                        Constants.ResultType.QUALITY -> {
                            for (inc in 0 until (res as ResultQuality).screenshotScores.size) {
                                res.screenshotScores[inc] += (toComp as ResultQuality).screenshotScores[inc]
                            }
                        }
                        Constants.ResultType.PLAYBACK -> {
                            (res as ResultPlayback).warnings += (toComp as ResultPlayback).warnings
                            (res as ResultPlayback).framesDropped += toComp.framesDropped
                        }
                        Constants.ResultType.SPEED -> {
                            (res as ResultSpeed).speed += (toComp as ResultSpeed).speed
                        }
                        else -> {
                            Log.e(TAG, "mergeResults: Unknown result type")
                            return null
                        }
                    }
                }
                when (res.type) {
                    Constants.ResultType.QUALITY -> {
                        for (inc in 0 until (res as ResultQuality).screenshotScores.size) {
                            res.screenshotScores[inc] /= results.size
                        }
                    }
                    Constants.ResultType.PLAYBACK -> {
                        (res as ResultPlayback).warnings /= results.size
                        (res as ResultPlayback).framesDropped /= results.size
                    }
                    Constants.ResultType.SPEED -> {
                        (res as ResultSpeed).speed /= results.size
                    }
                    else -> {
                        Log.e(TAG, "mergeResults: Unknown result type")
                        return null
                    }
                }
                mergedResults.add(res)
            }
            return mergedResults
        }
    }
}