package org.videolan.vlcbenchmark.results.types

import kotlinx.android.parcel.Parcelize
import org.json.JSONObject
import org.videolan.vlcbenchmark.Constants
import org.videolan.vlcbenchmark.results.ResultModel

@Parcelize
class ResultSpeed(
        override var name: String = "",
        override var hardware: Boolean = false,
        override var score: Double = 50.0,
        override var maxScore: Double = 50.0,
        override var type: Constants.ResultType = Constants.ResultType.SPEED,
        override var crash: String = "",
        override var stacktrace: String = "",
        var speed: Double = 1.0
) : ResultModel(name, hardware, score, maxScore, type, crash, stacktrace) {
    constructor(jsonObject: JSONObject) : this() {
        this.name = jsonObject.getString("name")
        this.score = jsonObject.getInt("score").toDouble()
        this.hardware = jsonObject.getBoolean("hardware")
        this.crash = jsonObject.getString("crash")
        this.stacktrace = jsonObject.getString("stacktrace")
        this.speed = jsonObject.getDouble("speed")
    }

    override fun jsonDump(): JSONObject {
        val jsonObject = super.jsonDump()
        jsonObject.put("speed", this.speed)
        jsonObject.put("max_score", this.maxScore)
        return jsonObject
    }

    fun setResults(speed: Double) {
        if (speed < 1.0)
            this.score *= speed
        this.speed = speed
    }

    companion object {
        @Suppress("UNUSED")
        private val TAG = this::class.java.name
    }
}