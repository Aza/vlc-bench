package org.videolan.vlcbenchmark.results

import kotlinx.android.parcel.Parcelize
import org.json.JSONObject
import org.videolan.vlcbenchmark.Constants
import java.util.*

@Parcelize
open class ResultModel (
   open var name: String = "",
   open var hardware: Boolean = false,
   open var score: Double = 0.0,
   open var maxScore: Double = 0.0,
   open var type: Constants.ResultType = Constants.ResultType.PLAYBACK,
   open var crash: String = "",
   open var stacktrace: String = ""
) : IResult {

    constructor(jsonObject: JSONObject) : this() {
        this.name = jsonObject.getString("name")
        this.score = jsonObject.getInt("score").toDouble()
        this.hardware = jsonObject.getBoolean("hardware")
        val type = jsonObject.getString("type")
        this.type = when (type) {
            "playback" -> Constants.ResultType.PLAYBACK
            "quality" -> Constants.ResultType.QUALITY
            "speed" -> Constants.ResultType.SPEED
            else -> Constants.ResultType.UNKNOWN
        }
        this.crash = jsonObject.getString("crash")
        this.stacktrace = jsonObject.getString("stacktrace")
    }

    override fun jsonDump() : JSONObject {
        val jsonObject = JSONObject()
        jsonObject.put("name", name)
        jsonObject.put("score", score.toInt())
        jsonObject.put("hardware", hardware)
        jsonObject.put("type", type)
        jsonObject.put("crash", crash)
        jsonObject.put("stacktrace", stacktrace)
        return jsonObject
    }

    override fun setFailure(crash: String, stacktrace: String) {
        this.score = 0.0
        this.crash = crash
        this.stacktrace = stacktrace
    }

    fun getPrettyTypeString() : String {
        val decoderMode = if (hardware) "Hardware" else "Software"
        return type.toString().toLowerCase(Locale.getDefault()).capitalize() + " " + decoderMode
    }

    fun clearStacktrace() {
        this.stacktrace = ""
    }
}