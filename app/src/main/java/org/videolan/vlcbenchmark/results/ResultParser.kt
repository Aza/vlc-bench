package org.videolan.vlcbenchmark.results

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import org.videolan.vlcbenchmark.Constants
import org.videolan.vlcbenchmark.Constants.ResultCodes
import org.videolan.vlcbenchmark.R
import org.videolan.vlcbenchmark.VLCWorkerModel
import org.videolan.vlcbenchmark.results.types.ResultPlayback
import org.videolan.vlcbenchmark.results.types.ResultQuality
import org.videolan.vlcbenchmark.results.types.ResultSpeed
import org.videolan.vlcbenchmark.tests.Test
import org.videolan.vlcbenchmark.tests.TestController
import org.videolan.vlcbenchmark.tests.types.TestQuality
import org.videolan.vlcbenchmark.tools.StorageManager.tmpStackTraceFile
import java.io.BufferedReader
import java.io.File
import java.io.FileInputStream
import java.io.InputStreamReader

class ResultParser {

     private fun getStacktrace(): String {
          var stacktrace = ""
          try {
               val stacktraceFile = File(tmpStackTraceFile)
               val fin = FileInputStream(stacktraceFile)
               val streamReader = InputStreamReader(fin)
               val bf = BufferedReader(streamReader)
               val stacktraceContent = StringBuilder()
               var line = bf.readLine()
               while (line != null) {
                    stacktraceContent.append(line)
                    stacktraceContent.append("\n")
                    line = bf.readLine()
               }
               stacktrace = stacktraceContent.toString()
               fin.close()
               streamReader.close()
               bf.close()
          } catch (e: Exception) {
               e.printStackTrace()
          }
          return stacktrace
     }

     private fun getResultInstance(test: Test) : ResultModel? {
          return when (test.type) {
               Constants.TestType.QUALITY -> ResultQuality(test.sample.name, test.hardware)
               Constants.TestType.PLAYBACK -> ResultPlayback(test.sample.name, test.hardware)
               Constants.TestType.SPEED -> ResultSpeed(test.sample.name, test.hardware)
               Constants.TestType.UNKNOWN -> null
          }
     }

     private fun getCrashMessage(data: Intent?, context: Context) : String {
          return if (data != null && data.hasExtra("Error")) {
               data.getStringExtra("Error")
          } else if (data != null) {
               context.getString(R.string.result_vlc_crash)
          } else {
               try {
                    val packageContext: Context = context.createPackageContext(context.getString(R.string.vlc_package_name), 0)
                    val preferences = packageContext.getSharedPreferences(VLCWorkerModel.SHARED_PREFERENCE, Context.MODE_PRIVATE)
                    preferences.getString(VLCWorkerModel.SHARED_PREFERENCE_STACK_TRACE, "")
               } catch (e: PackageManager.NameNotFoundException) {
                    e.message;""
               }
          }
     }

     fun parse(context: Context, data: Intent?, resultCode: Int, testController: TestController) : ResultModel? {
          val errorMessage: String
          var stacktrace = ""
          val result = getResultInstance(testController.getCurrentTest()) ?: return null
          if (resultCode != ResultCodes.RESULT_OK) {
               errorMessage = when (resultCode) {
                    ResultCodes.RESULT_CANCELED -> context.getString(R.string.result_canceled)
                    ResultCodes.RESULT_NO_HW -> context.getString(R.string.result_no_hw)
                    ResultCodes.RESULT_CONNECTION_FAILED -> context.getString(R.string.result_connection_failed)
                    ResultCodes.RESULT_PLAYBACK_ERROR -> context.getString(R.string.result_playback_error)
                    ResultCodes.RESULT_HARDWARE_ACCELERATION_ERROR -> context.getString(R.string.result_hardware_acceleration_error)
                    ResultCodes.RESULT_VIDEO_TRACK_LOST -> context.getString(R.string.result_video_track_lost)
                    ResultCodes.RESULT_VLC_CRASH -> getCrashMessage(data, context)
                    else -> context.getString(R.string.result_unknown)
               }
               if (resultCode == ResultCodes.RESULT_VLC_CRASH) {
                    stacktrace = getStacktrace()
               }
               result.setFailure(errorMessage, stacktrace)
          } else {
               when (result) {
                    is ResultPlayback -> {
                         result.setResults(
                                 data!!.getIntExtra("number_of_dropped_frames", 0),
                                 data.getIntExtra("late_frames", 0)
                         )
                    }
                    is ResultQuality -> {
                         result.setResults(testController.getCurrentTest() as TestQuality, testController.currentIndex)
                    }
                    is ResultSpeed -> {
                         result.setResults(data!!.getFloatExtra("speed", 0.0f).toDouble())
                    }
               }
          }
          return result
     }

     companion object {
          @Suppress("UNUSED")
          private val TAG = this::class.java.name
     }
}