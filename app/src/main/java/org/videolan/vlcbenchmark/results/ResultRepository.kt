package org.videolan.vlcbenchmark.results

import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.videolan.vlcbenchmark.BuildConfig
import org.videolan.vlcbenchmark.Constants
import org.videolan.vlcbenchmark.SystemPropertiesProxy
import org.videolan.vlcbenchmark.results.types.ResultPlayback
import org.videolan.vlcbenchmark.results.types.ResultQuality
import org.videolan.vlcbenchmark.results.types.ResultSpeed
import org.videolan.vlcbenchmark.tools.FormatStr.dateStr
import org.videolan.vlcbenchmark.tools.StorageManager
import org.videolan.vlcbenchmark.tools.StorageManager.checkFolderLocation
import org.videolan.vlcbenchmark.tools.StorageManager.delete
import org.videolan.vlcbenchmark.tools.StorageManager.getInternalDirStr
import org.videolan.vlcbenchmark.tools.VLCProxy.Companion.getVLCVersion
import java.io.*

class ResultRepository {

    fun getCurrentResultList(context: Context) : Array<ArrayList<ResultModel>> {
        val jsonFile = File(context.filesDir, filename)
        val loopList: Array<ArrayList<ResultModel>>
        try {
            val text = StringBuilder()
            val br = BufferedReader(FileReader(jsonFile))
            var line: String?
            while (br.readLine().also { line = it } != null) {
                text.append(line)
                text.append('\n')
            }
            br.close()
            val jsonArray = JSONArray(text.toString())
            Log.w(TAG, jsonArray.toString())
            loopList = if (jsonArray.length() == 1) {
                arrayOf(ArrayList())
            } else {
                arrayOf(ArrayList(), ArrayList(), ArrayList())
            }
            for (i in 0 until jsonArray.length()) {
                val benchmark = jsonArray.getJSONObject(i)
                if (benchmark.has("playback")) {
                    val playbackArray = benchmark.getJSONArray("playback")
                    for (j in 0 until playbackArray.length()) {
                        val jsonObject = JSONObject(playbackArray[j].toString())
                        loopList[i].add(ResultPlayback(jsonObject))
                    }
                }
                if (benchmark.has("quality")) {
                    val qualityArray = benchmark.getJSONArray("quality")
                    for (j in 0 until qualityArray.length()) {
                        val jsonObject = JSONObject(qualityArray[j].toString())
                        loopList[i].add(ResultQuality(jsonObject))
                    }
                }
                if (benchmark.has("speed")) {
                    val speedArray = benchmark.getJSONArray("speed")
                    for (j in 0 until speedArray.length()) {
                        val jsonObject = JSONObject(speedArray[j].toString())
                        loopList[i].add(ResultSpeed(jsonObject))
                    }
                }
                loopList[i].sortWith(compareBy(ResultModel::name, ResultModel::hardware, ResultModel::type))
            }
        } catch (e: FileNotFoundException) {
            Log.i(TAG, "Json file not found: $e")
            return arrayOf()
        } catch (e: JSONException) {
            Log.e(TAG, "Failed to load json file : $e")
            return arrayOf()
        } catch (e: IOException) {
            Log.e(TAG, "Failed to read jsonFile : $e")
            return arrayOf()
        }
        Log.w(TAG, "getCurrentResultList: looplist len: ${loopList.size}")
        return loopList
    }

    fun setCurrentResultList(context: Context, resultArray: Array<ArrayList<ResultModel>>) : String {
        val jsonFile = File(context.filesDir, filename)
        try {
            val jsonTestInformation = JSONArray()
            for (resultList in resultArray) {
                val jsonTestResults = JSONObject()
                val qualityResults = JSONArray()
                val playbackResults = JSONArray()
                val speedResults = JSONArray()
                for (result in resultList) {
                    when (result.type) {
                        Constants.ResultType.QUALITY -> qualityResults.put((result as ResultQuality).jsonDumpWithScreenshots())
                        Constants.ResultType.PLAYBACK -> playbackResults.put((result as ResultPlayback).jsonDump())
                        Constants.ResultType.SPEED -> speedResults.put((result as ResultSpeed).jsonDump())
                        else -> {
                            Log.e(TAG, "setCurrentResultList: Failed to get correct result type")
                            return ""
                        }
                    }
                }
                jsonTestResults.put("quality", qualityResults)
                jsonTestResults.put("playback", playbackResults)
                jsonTestResults.put("speed", speedResults)
                jsonTestInformation.put(jsonTestResults)
            }
            val jsonFileOutputStream = FileOutputStream(jsonFile, false)
            if (BuildConfig.DEBUG) {
                jsonFileOutputStream.write(jsonTestInformation.toString().toByteArray())
            } else {
                jsonFileOutputStream.write(jsonTestInformation.toString().toByteArray())
            }
        } catch (e: IOException) {
            Log.e(TAG, "Failed to save json test results")
            delete(jsonFile)
            return ""
        } catch (e: JSONException) {
            Log.e(TAG, "Failed to save json test results")
            delete(jsonFile)
            return ""
        }
        return filename
    }

    fun addToCurrentResultList(context: Context, result: ResultModel, loopIndex: Int) {
        val resultArray = getCurrentResultList(context)
        resultArray[loopIndex].add(result)
        setCurrentResultList(context, resultArray)
    }

    fun discardCurrentResultList(context: Context) {
        val jsonFile = File(context.filesDir, filename)
        jsonFile.delete()
    }

    @Throws(JSONException::class)
    fun saveResultList(results: ArrayList<ResultModel>): String? {
        val testInformation: JSONObject = getBenchmarkResultsJSON(results, true) ?: return null
        val jsonFileOutputStream: FileOutputStream
        val folderName = getInternalDirStr(StorageManager.jsonFolder)
        if (!checkFolderLocation(folderName)) {
            Log.e(TAG, "Failed to created json folder")
            return null
        }
        val fileName = dateStr
        val jsonFile = File("$folderName$fileName.txt")
        try {
            jsonFileOutputStream = FileOutputStream(jsonFile)
            if (BuildConfig.DEBUG) {
                jsonFileOutputStream.write(testInformation.toString(4).toByteArray())
            } else {
                jsonFileOutputStream.write(testInformation.toString().toByteArray())
            }
        } catch (e: IOException) {
            Log.e(TAG, "Failed to save json test results")
            delete(jsonFile)
            return null
        }
        return fileName
    }

    fun getResultList(id: String) : ArrayList<ResultModel> {
        val jsonFile = File(getInternalDirStr(StorageManager.jsonFolder) + id)
        val resultList = ArrayList<ResultModel>()
        try {
            val text = java.lang.StringBuilder()
            val br = BufferedReader(FileReader(jsonFile))
            var line: String?
            while (br.readLine().also { line = it } != null) {
                text.append(line)
                text.append('\n')
            }
            br.close()
            val benchmark = JSONObject(text.toString())
            if (benchmark.has("playback")) {
                val playbackArray = benchmark.getJSONArray("playback")
                for (j in 0 until playbackArray.length()) {
                    val jsonObject = JSONObject(playbackArray[j].toString())
                    resultList.add(ResultPlayback(jsonObject))
                }
            }
            if (benchmark.has("quality")) {
                val qualityArray = benchmark.getJSONArray("quality")
                for (j in 0 until qualityArray.length()) {
                    val jsonObject = JSONObject(qualityArray[j].toString())
                    resultList.add(ResultQuality(jsonObject))
                }
            }
            if (benchmark.has("speed")) {
                val speedArray = benchmark.getJSONArray("speed")
                for (j in 0 until speedArray.length()) {
                    val jsonObject = JSONObject(speedArray[j].toString())
                    resultList.add(ResultSpeed(jsonObject))
                }
            }
            resultList.sortWith(compareBy(ResultModel::name, ResultModel::hardware, ResultModel::type))
        } catch (e: FileNotFoundException) {
            Log.e(TAG, "Json file not found: $e")
            return ArrayList()
        } catch (e: JSONException) {
            Log.e(TAG, "Failed to load json file : $e")
            return ArrayList()
        } catch (e: IOException) {
            Log.e(TAG, "Failed to read jsonFile : $e")
            return ArrayList()
        }
        return resultList
    }

    fun getAllResultListIds() : ArrayList<String> {
        val dirname = getInternalDirStr(StorageManager.jsonFolder) ?: return ArrayList()
        val dir = File(dirname)
        val files = dir.listFiles()
        val results = java.util.ArrayList<String>()
        if (files != null) {
            for (file in files) {
                if (file.name.contains(".txt")) {
                    results.add(file.name.replace(".txt".toRegex(), ""))
                }
            }
        }
        return results
    }

    companion object {
        private const val TAG = "ResultRepository"
        private const val filename = "save.json"

        fun dumpResults(context: Context, results: ArrayList<ResultModel>, gpuData: Intent, withScreenshots: Boolean): JSONObject? {
            val jsonObject = JSONObject()
            var softwareScore = 0
            var hardwareScore = 0

            val jsonDevice: JSONObject = getDeviceInformationJSON(gpuData)
            val jsonResults: JSONObject = getBenchmarkResultsJSON(results, withScreenshots)
                    ?: return null

            for (result in results) {
                softwareScore += if (result.hardware) 0 else result.score.toInt()
                hardwareScore += if (result.hardware) result.score.toInt() else 0
            }

            jsonObject.put("device_information", jsonDevice)
            jsonObject.put("results", jsonResults)
            jsonObject.put("score_software", softwareScore)
            jsonObject.put("score_hardware", hardwareScore)
            jsonObject.put("vlc_version", getVLCVersion(context))
            return jsonObject
        }

        /**
         * Returns benchmark results in a JSONArray.
         *
         * @param results list of all benchmark results.
         * @return null in case of failure.
         */
        @Throws(JSONException::class)
        private fun getBenchmarkResultsJSON(resultList: ArrayList<ResultModel>, withScreenshot: Boolean): JSONObject? {
            val jsonTestResults = JSONObject()
            val qualityResults = JSONArray()
            val playbackResults = JSONArray()
            val speedResults = JSONArray()
            for (result in resultList) {
                when (result.type) {
                    Constants.ResultType.QUALITY -> {
                        if (withScreenshot)
                            qualityResults.put((result as ResultQuality).jsonDumpWithScreenshots())
                        else
                            qualityResults.put((result as ResultQuality).jsonDump())
                    }
                    Constants.ResultType.PLAYBACK -> playbackResults.put((result as ResultPlayback).jsonDump())
                    Constants.ResultType.SPEED ->speedResults.put(((result) as ResultSpeed).jsonDump())
                    else -> {
                        Log.e(TAG, "setCurrentResultList: Failed to get correct result type")
                        return null
                    }
                }
            }
            jsonTestResults.put("quality", qualityResults)
            jsonTestResults.put("playback", playbackResults)
            jsonTestResults.put("speed", speedResults)
            return jsonTestResults
        }


        /**
         * Returns device information in a JSONObject.
         *
         * @return null in case of failure.
         */
        @Throws(JSONException::class)
        private fun getDeviceInformationJSON(gpuData: Intent): JSONObject {
            val properties = JSONObject()
            properties.put("board", Build.BOARD)
            properties.put("bootloader", Build.BOOTLOADER)
            properties.put("brand", Build.BRAND)
            properties.put("device", Build.DEVICE)
            properties.put("display", Build.DISPLAY)
            properties.put("fingerprint", Build.FINGERPRINT)
            properties.put("id", Build.ID)
            properties.put("manufacturer", Build.MANUFACTURER)
            properties.put("model", Build.MODEL)
            properties.put("product", Build.PRODUCT)
            properties.put("serial", Build.SERIAL)
            properties.put("supported_32_bit_abi", JSONArray(Build.SUPPORTED_32_BIT_ABIS))
            properties.put("supported_64_bit_abi", JSONArray(Build.SUPPORTED_64_BIT_ABIS))
            properties.put("tags", Build.TAGS)
            properties.put("type", Build.TYPE)
            properties.put("os_arch", System.getProperty("os.arch"))
            properties.put("kernel_name", System.getProperty("os.name"))
            properties.put("kernel_version", System.getProperty("os.version"))
            properties.put("version", Build.VERSION.RELEASE)
            properties.put("sdk", Build.VERSION.SDK_INT.toString())
            properties.put("cpu_model", SystemPropertiesProxy.getCpuModel())
            properties.put("cpu_cores", SystemPropertiesProxy.getCpuCoreNumber())
            properties.put("cpu_min_freq", SystemPropertiesProxy.getCpuMinFreq())
            properties.put("cpu_max_freq", SystemPropertiesProxy.getCpuMaxFreq())
            properties.put("total_ram", SystemPropertiesProxy.getRamTotal())
            properties.put("gpu_model", gpuData.getStringExtra("gl_renderer"))
            properties.put("gpu_vendor", gpuData.getStringExtra("gl_vendor"))
            properties.put("opengl_version", gpuData.getStringExtra("gl_version"))
            properties.put("opengl_extensions", gpuData.getStringExtra("gl_extensions"))
            return properties
        }
    }
}