package org.videolan.vlcbenchmark

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import org.videolan.vlcbenchmark.Constants.ResultType
import org.videolan.vlcbenchmark.results.ResultModel
import org.videolan.vlcbenchmark.results.types.ResultPlayback
import org.videolan.vlcbenchmark.results.types.ResultQuality
import org.videolan.vlcbenchmark.results.types.ResultSpeed
import org.videolan.vlcbenchmark.tools.FormatStr.format2Dec

class ResultDetailDialog : DialogFragment() {

    var result: ResultModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.layout_result_detail_dialog, container, false)

        if (arguments != null && arguments!!.get("result") == null) {
            Toast.makeText(activity, R.string.toast_error_result_detail_error, Toast.LENGTH_SHORT).show()
            dismiss()
        }

        result = arguments!!.getParcelable("result")
        val layoutParams: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT)

        view.findViewById<TextView>(R.id.detail_sample).text = result!!.name
        view.findViewById<TextView>(R.id.detail_type).text = result!!.getPrettyTypeString()

        view.findViewById<TextView>(R.id.detail_score).text =
                String.format(getString(R.string.detail_score), result!!.score.toInt(), result!!.maxScore.toInt())
        when (result!!.type) {
            ResultType.QUALITY -> {
                view.findViewById<LinearLayout>(R.id.detail_layout_playback).visibility = View.GONE
                view.findViewById<LinearLayout>(R.id.detail_layout_speed).visibility = View.GONE
                view.findViewById<TextView>(R.id.detail_quality_bad_screenshots).text =
                        String.format(getString(R.string.detail_bad_screenshots),
                                format2Dec((result as ResultQuality).percentOfBadScreenshot))
                layoutParams.addRule(RelativeLayout.BELOW, R.id.detail_layout_quality)
            }
            ResultType.PLAYBACK -> {
                view.findViewById<LinearLayout>(R.id.detail_layout_quality).visibility = View.GONE
                view.findViewById<LinearLayout>(R.id.detail_layout_speed).visibility = View.GONE
                view.findViewById<TextView>(R.id.detail_playback_frames_dropped).text =
                        String.format(getString(R.string.detail_frames_dropped),
                                (result as ResultPlayback).framesDropped)
                view.findViewById<TextView>(R.id.detail_playback_warnings).text =
                        String.format(getString(R.string.detail_warning_number),
                                (result as ResultPlayback).warnings)
                layoutParams.addRule(RelativeLayout.BELOW, R.id.detail_layout_playback)
            }
            ResultType.SPEED -> {
                view.findViewById<LinearLayout>(R.id.detail_layout_quality).visibility = View.GONE
                view.findViewById<LinearLayout>(R.id.detail_layout_playback).visibility = View.GONE
                view.findViewById<TextView>(R.id.detail_speed_speed).text =
                        String.format(getString(R.string.detail_speed), (result as ResultSpeed).speed)
                layoutParams.addRule(RelativeLayout.BELOW, R.id.detail_layout_speed)
            }
            ResultType.UNKNOWN -> {
                Toast.makeText(activity, R.string.toast_error_result_detail_error, Toast.LENGTH_SHORT).show()
                dismiss()
            }
        }
        if (result!!.crash != "") {
            val crashText = view.findViewById<TextView>(R.id.detail_crash)
            crashText.text = String.format(getString(R.string.detail_crash), result!!.crash)
            crashText.layoutParams = layoutParams
        } else {
            view.findViewById<TextView>(R.id.detail_crash).visibility = View.INVISIBLE
        }

        return view
    }
}