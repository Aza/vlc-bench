/*
 *****************************************************************************
 * VLCWorkerModel.java
 *****************************************************************************
 * Copyright © 2016-2018 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

package org.videolan.vlcbenchmark;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.UiThread;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import org.json.JSONException;
import org.videolan.vlcbenchmark.api.RetrofitInstance;
import org.videolan.vlcbenchmark.benchmark.BenchmarkViewModel;
import org.videolan.vlcbenchmark.benchmark.ScreenshotService;
import org.videolan.vlcbenchmark.results.ResultController;
import org.videolan.vlcbenchmark.results.ResultModel;
import org.videolan.vlcbenchmark.results.ResultParser;
import org.videolan.vlcbenchmark.results.ResultRepository;
import org.videolan.vlcbenchmark.tests.Test;
import org.videolan.vlcbenchmark.tests.TestController;
import org.videolan.vlcbenchmark.tests.types.TestPlayback;
import org.videolan.vlcbenchmark.tests.types.TestQuality;
import org.videolan.vlcbenchmark.tests.types.TestSpeed;
import org.videolan.vlcbenchmark.tools.CrashHandler;
import org.videolan.vlcbenchmark.tools.DialogInstance;
import org.videolan.vlcbenchmark.tools.GoogleConnectionHandler;
import org.videolan.vlcbenchmark.tools.StorageManager;
import org.videolan.vlcbenchmark.tools.Util;

import java.util.ArrayList;


/**
 * <p>
 * Main class of the project.
 * This class handle the whole logic/algorithm side of the application.
 * <p>
 * It extends from Activity yet it doesn't touch anything related to UI except {@link VLCWorkerModel#onActivityResult(int, int, Intent)}
 * and at some specific points the termination of the activity.
 * <p>
 * This class cannot be instantiated directly it requires to be extended and to implement all of its abstract method,
 * its in those methods only that the UI part of the activity can be handled.
 * <p>
 * This architecture allows the UI and logical part to be independent from one an other.
 */
public abstract class VLCWorkerModel extends AppCompatActivity {

    private final static String TAG = "VLCWorkerModel";

    private Intent mData = null;
    private int mResultCode = -2;

    protected BenchmarkViewModel model;

    public static final String SHARED_PREFERENCE = "org.videolab.vlc.gui.video.benchmark.UNCAUGHT_EXCEPTIONS";
    public static final String SHARED_PREFERENCE_STACK_TRACE = "org.videolab.vlc.gui.video.benchmark.STACK_TRACE";

    private static final String STATE_RUNNING = "STATE_RUNNING";
    private static final String STATE_TEST_CONTROLLER = "STATE_TEST_CONTROLLER";
    private static final String STATE_RESULT_CONTROLLER = "STATE_RESULT_CONTROLLER";
    private static final String STATE_ORIENTATION = "STATE_ORIENTATION";

    protected abstract void fakeInput();
    protected abstract boolean setCurrentFragment(int itemId);
    public abstract void dismissDialog();

    /**
     * Is called during the {@link VLCWorkerModel#onCreate(Bundle)}.
     */
    protected abstract void setupUiMembers(Bundle savedInstanceState);

    /**
     * Called to update the test dialog.
     * @param progress benchmark progress percentage
     * @param progressText text recaping the progress state of the benchmark:
     *                     file index, loop number, etc
     * @param sampleName the name of the test (ex : screenshot software, ...)
     */
    protected abstract void updateProgress(double progress, String progressText, String sampleName);

    @Override
    protected void onStart() {
        super.onStart();
        bindToService();
    }

    private void bindToService() {
        if (model.getRunning()) {
            Intent intent = new Intent(this, ScreenshotService.class);
            startService(intent);
            Intent boundIntent = new Intent(this, ScreenshotService.class);
            bindService(boundIntent, connection, BIND_AUTO_CREATE);
        }
    }

    private void unbindFromService() {
        if (mBound) {
            unbindService(connection);
            mBound = false;
        }
    }

    private void stopService() {
        if (mService != null) {
            mService.stopSelf();
            unbindFromService();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindFromService();
    }

    ScreenshotService mService;
    boolean mBound = false;
    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            ScreenshotService.ScreenshotServiceBinder binder = (ScreenshotService.ScreenshotServiceBinder) iBinder;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBound = false;
        }
    };

    /**
     * Initialization of the Activity.
     *request permissions to read on the external storage.
     *
     * @param savedInstanceState saved state bundle
     */
    @Override
    final protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CrashHandler.setCrashHandler();
        model = ViewModelProviders.of(this).get(BenchmarkViewModel.class);

        StorageManager.INSTANCE.setStoragePreference(this);
        setupUiMembers(savedInstanceState);

        RetrofitInstance.init(getString(R.string.build_api_address));
        GoogleConnectionHandler.getInstance();
    }

    @UiThread
    final public void startBenchmark(TestController testController, ResultController resultController) {
        model.setRunning(true);
        model.testController = testController;
        model.resultController = resultController;
        bindToService();
        setupScreenshotCallbacks();
    }

    private void setupScreenshotCallbacks() {
        model.setProjectionManager((MediaProjectionManager)getSystemService(Context.MEDIA_PROJECTION_SERVICE));
        if (model.getProjectionManager() == null) {
            Log.e(TAG, "launchTests: Failed to create MediaProjectionManager");
            stopBenchmark();
            return;
        }
        Intent intent = new Intent(this, ScreenshotService.class);
        startService(intent);
        Intent boundIntent = new Intent(this, ScreenshotService.class);
        bindService(boundIntent, connection, BIND_AUTO_CREATE);

        Intent screenshotIntent = model.getProjectionManager().createScreenCaptureIntent();
        screenshotIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION & Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
        startActivityForResult(screenshotIntent, Constants.RequestCodes.SCREENSHOT);
    }

    /**
     * This method creates a new intent that corresponds with VLC's BenchActivity launch protocol.
     * Then calls the listener method to continue the benchmark flow
     */
    private void createIntentForVlc(OnIntentCreatedListener listener) {
        Test test = model.getTestController().getCurrentTest();
        switch (test.getType()) {
            case PLAYBACK:
                ((TestPlayback)test).prepareIntent(this, new Intent(), listener);
                break;
            case QUALITY:
                ((TestQuality)test).prepareIntent(this, new Intent(), listener);
                break;
            case SPEED:
                ((TestSpeed)test).prepareIntent(this, new Intent(), listener);
                break;
            case UNKNOWN:
                break;
        }
    }

    public interface OnIntentCreatedListener {
        void onItentCreated(Intent intent);
    }

    /**
     * Re-entry point, in this method we receive directly from VLC its
     * result code along with an Intent giving extra information about the result of VLC.
     * <p>
     * This method will be called a each end of a test and will therefor handle the launch of the next tests.
     * It also handle that case were crashed without notice by checking if the Intent in argument if null and if so
     * by getting the String describing the crashed VLC had by reading into VLC's shared preferences.
     * <p>
     * This method also calls a number of abstract method to allow the UI to update itself.
     *
     * @param requestCode the code we gave to VLC to launch itself.
     * @param resultCode  the code on which VLC finished.
     * @param data an Intent describing additional information and data about the test.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) { //TODO refactor all this, lots of useless stuff
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.RequestCodes.VLC) {
            Log.i(TAG, "onActivityResult: resultCode: " + resultCode );
             mData = data;
             mResultCode = resultCode;
        } else if (requestCode == Constants.RequestCodes.GOOGLE_CONNECTION) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.main_page_fragment_holder);
            if (fragment != null) {
                fragment.onActivityResult(requestCode, resultCode, data);
            } else {
                Log.e(TAG, "onActivityResult: GOOGLE_CONNECTION: fragment is null");
            }
        } else if (requestCode == Constants.RequestCodes.SCREENSHOT && resultCode == RESULT_OK) {
            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
            MediaProjection mediaProjection = model.getProjectionManager().getMediaProjection(resultCode, data);
            if (mediaProjection == null) {
                Log.e(TAG, "onActivityResult: Failed to create MediaProjection" );
                return;
            }
            if (mBound) {
                // Most of the time when setting up screen dimensions, the orientation will be
                // portrait mode, but the screenshots are taken in landscape mode.
                // It's important to invert dimension values when in portrait mode.
                if (metrics.widthPixels >= metrics.heightPixels) {
                    mService.setup(
                            metrics.widthPixels,
                            metrics.heightPixels,
                            metrics.densityDpi,
                            mediaProjection
                    );
                } else {
                    mService.setup(
                            metrics.heightPixels,
                            metrics.widthPixels,
                            metrics.densityDpi,
                            mediaProjection
                    );
                }
            } else {
                Log.e(TAG, "onActivityResult: Service not bound");
            }

            model.setOrientation(this.getResources().getConfiguration().orientation);
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            try {
                createIntentForVlc(intent -> {
                    /* In case of failure due to an invalid file, stop benchmark, and display download page */
                    if (intent == null) {
                        dismissDialog();
                        Log.e(TAG, "launchTests: " + getString(R.string.dialog_text_invalid_file ));
                        new DialogInstance(R.string.dialog_title_error, R.string.dialog_text_invalid_file).display(VLCWorkerModel.this);
                        setCurrentFragment(R.id.home_nav);
                        return;
                    }
                    startActivityForResult(intent, Constants.RequestCodes.VLC);
                });

            } catch (ActivityNotFoundException e) {
                new DialogInstance(R.string.dialog_title_error, R.string.dialog_text_vlc_failed).display(this);
                Log.e(TAG, "launchTests: Failed to start VLC");
            }
        }
    }

    private void updateDialogProgress() {
        updateProgress(model.testController.getProgress(model.resultController.getMaxLoop()),
                model.testController.getProgressString(this,
                        model.resultController.getCurrentLoopIndex(),
                        model.resultController.getMaxLoop()),
                model.testController.getCurrentTest().getSample().getName());
    }

    /**
     * This method increment all the counters relatives to the type of test we will do,
     * such as the type of test we should do, the index of the file we're testing and
     * on what loop are we.
     * <p>
     * If we reached the end of the tests we then calculate the average score for hardware and software
     * call the abstract method {@link VLCWorkerModel#onTestsFinished()}  and return
     * <p>
     * Otherwise we launch VLC's BenchActivity with the counters' new values.
     */
    private void launchNextTest() {
        if (model.getRunning()) {
            fakeInput();
            if (!model.testController.next()) {
                onTestsFinished();
                return;
            }
            updateDialogProgress();
            createIntentForVlc(intent -> {
                if (intent == null) {
                    dismissDialog();
                    model.setRunning(false);
                    new DialogInstance(R.string.dialog_title_error, R.string.dialog_text_invalid_file).display(VLCWorkerModel.this);
                    setCurrentFragment(R.id.home_nav);
                    return;
                }
                // Add delay for vlc to finish correctly
                Handler handler = new Handler();
                handler.postDelayed(() -> {
                    if (model.getRunning()) {
                        startActivityForResult(intent, Constants.RequestCodes.VLC);
                    }
                }, 4000);
            });

        } else {
            Log.e(TAG, "launchNextTest was called but running is false.");
        }
    }

    void startResultPage(String name) {
        if (name == null) {
            new DialogInstance(R.string.dialog_title_oups, R.string.dialog_text_save_failure)
                    .display(this);
            return;
        }
        Intent intent = new Intent(VLCWorkerModel.this, ResultPage.class);
        intent.putExtra("name", name);
        intent.putExtra("fromBench", true);
        startActivityForResult(intent, Constants.RequestCodes.RESULTS);
    }

    private void onTestsFinished() {
        ResultRepository resultRepository = new ResultRepository();
        ArrayList<ResultModel>[] results = resultRepository.getCurrentResultList(this);
        ArrayList<ResultModel> sanitizedResults = ResultController.Companion.mergeResults(results);
        dismissDialog();
        model.setRunning(false);
        resultRepository.discardCurrentResultList(this);
        stopBenchmark();
        if (sanitizedResults == null) {
            Log.e(TAG, "onTestsFinished: Failed to save benchmark results");
            Toast.makeText(this, R.string.dialog_text_saving_results_failure, Toast.LENGTH_LONG).show();
            return;
        }

        Util.runInBackground(() -> {
            String savedName = null;
            try {
                savedName = resultRepository.saveResultList(sanitizedResults);
            } catch (JSONException e) {
                Log.e(TAG, "Failed to save test : " + e.toString());
            }
            final String name = savedName;
            stopService();
            Util.runInUiThread(() -> startResultPage(name));
        });

    }

    protected void stopBenchmark() {
        stopService();
        model.setRunning(false);
        this.setRequestedOrientation(model.getOrientation());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (model.getRunning()) {
            updateDialogProgress();

            // -2 isn't return by vlc-android
            // small hack to stop the benchmark from restarting vlc if there is a
            // configuration change between onResume and vlc starting
            if (mResultCode != -2) {
                Util.runInBackground(() -> {
                    ResultModel result = new ResultParser().parse(this, mData, mResultCode, model.testController);
                    if (result == null) {
                        Toast.makeText(this, R.string.toast_error_result_parse, Toast.LENGTH_LONG).show();
                        stopBenchmark();
                        return;
                    }
                    model.resultController.addResults(this, result);
                    mResultCode = -2;
                    Util.runInUiThread(this::launchNextTest);
                });
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (model.getRunning()) {
            outState.putBoolean(STATE_RUNNING, model.getRunning());
            outState.putParcelable(STATE_TEST_CONTROLLER, model.testController);
            outState.putParcelable(STATE_RESULT_CONTROLLER, model.resultController);
            outState.putInt(STATE_ORIENTATION, model.getOrientation());
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        TestController testController;
        ResultController resultController;

        model.setRunning(savedInstanceState.getBoolean(STATE_RUNNING));
        model.setOrientation(savedInstanceState.getInt(STATE_ORIENTATION));
        if (model.getRunning()) {
            testController = savedInstanceState.getParcelable(STATE_TEST_CONTROLLER);
            if (testController != null)
                model.testController = testController;

            resultController = savedInstanceState.getParcelable(STATE_RESULT_CONTROLLER);
            if (resultController != null)
                model.resultController = resultController;
        }
    }
}
