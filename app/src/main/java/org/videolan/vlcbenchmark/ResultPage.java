/*
 *****************************************************************************
 * ResultPage.java
 *****************************************************************************
 * Copyright © 2016-2018 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

package org.videolan.vlcbenchmark;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.videolan.vlcbenchmark.api.ApiCalls;
import org.videolan.vlcbenchmark.results.ResultModel;
import org.videolan.vlcbenchmark.results.ResultRepository;
import org.videolan.vlcbenchmark.tools.FormatStr;
import org.videolan.vlcbenchmark.tools.GoogleConnectionHandler;
import org.videolan.vlcbenchmark.tools.StorageManager;
import org.videolan.vlcbenchmark.tools.Util;

import java.util.ArrayList;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ResultPage extends AppCompatActivity {

    private final static String TAG = ResultPage.class.getName();

    ArrayList<ResultModel> results;
    String benchmarkName;
    RecyclerView mRecyclerView = null;

    private int softwareScore = 0;
    private int hardwareScore = 0;
    private boolean hasSendData = true;

    private GoogleConnectionHandler mGoogleConnectionHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_page);

        setupUi();
        if (getIntent().getBooleanExtra("fromBench", false)) {
            hasSendData = false;
        }
    }

    private void setupUi() {
        if (!getIntent().hasExtra("name")) {
            Log.e(TAG, "setupUi: Failed to get name extra in intent");
            return;
        }
        benchmarkName = getIntent().getStringExtra("name");

        Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        if (toolbar == null) {
            Log.e(TAG, "setupUi: Failed to get action bar");
            return;
        }
        toolbar.setTitle(FormatStr.INSTANCE.toDatePrettyPrint(benchmarkName));
        setSupportActionBar(toolbar);

//        results = JsonHandler.load(benchmarkName + ".txt");
        results = new ResultRepository().getResultList(benchmarkName + ".txt");
        if (results.isEmpty()) {
            Log.e(TAG, "setupUi: Failed to get results from file");
            return;
        }

        for (ResultModel result : results) {
            softwareScore += result.getHardware() ? 0 : result.getScore();
            hardwareScore += result.getHardware() ? result.getScore() : 0;
        }
        RecyclerView.LayoutManager mLayoutManager;
        RecyclerView.Adapter mAdapter;

        TextView softView = (TextView) findViewById(R.id.softAvg);
        String softText = "Software score : " + FormatStr.INSTANCE.format2Dec(softwareScore);
        softView.setText(softText);

        TextView hardView = (TextView) findViewById(R.id.hardAvg);
        String hardText = "Hardware score : " + FormatStr.INSTANCE.format2Dec(hardwareScore);
        hardView.setText(hardText);

        mRecyclerView = (RecyclerView) findViewById(R.id.test_result_list);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView .setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        mAdapter = new ResultListAdapter(results);
        mRecyclerView.setAdapter(mAdapter);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (NullPointerException e) {
            Log.e(TAG, e.toString());
        }

        if (BuildConfig.BUILD_TYPE.equals("debug") || BuildConfig.BUILD_TYPE.equals("debug_prod")) {
            /* Sending JSON results to server */
            /* But need to connect to google first to get user id */
            Button button = (Button) findViewById(R.id.uploadButton);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mGoogleConnectionHandler.isConnected()) {
                        startActivityForResult(new Intent(ResultPage.this, BenchGLActivity.class),
                                Constants.RequestCodes.OPENGL);
                    } else {
                        mGoogleConnectionHandler.signIn();
                    }
                }
            });

            View separator = findViewById(R.id.result_page_separator);
            separator.setVisibility(View.VISIBLE);

            button.setVisibility(View.VISIBLE);
        }
    }

    JSONObject addGoogleUser(JSONObject jsonObject) throws JSONException{
        if (mGoogleConnectionHandler != null && mGoogleConnectionHandler.getAccount() != null) {
            jsonObject.put("email", mGoogleConnectionHandler.getAccount().getEmail());
            return jsonObject;
        } else {
            if (mGoogleConnectionHandler == null) {
                Log.d(TAG, "onActivityResult: mGoogleConnectionHandler is null");
            } else {
                Log.e(TAG, "onActivityResult: Failed to get google email");
            }
            Toast.makeText(this, R.string.dialog_text_err_google, Toast.LENGTH_LONG).show();
            return null;
        }
    }

    void prepareBenchmarkUpload(Boolean withScreenshots, Intent data) {
        try {
            JSONObject jsonObject = ResultRepository.Companion.dumpResults(this, results, data, withScreenshots);
            jsonObject = addGoogleUser(jsonObject);
            if (withScreenshots) {
                ApiCalls.uploadBenchmarkWithScreenshots(this, jsonObject, results);
            } else {
                ApiCalls.uploadBenchmark(this, jsonObject);
            }

        } catch (JSONException e) {
            Log.e(TAG, e.toString());
            Toast.makeText(this, R.string.toast_text_error_prep_upload, Toast.LENGTH_LONG).show();
        }
    }

    void startUploadDialog(Intent data) {
        String directory = StorageManager.INSTANCE.getDirectory() + StorageManager.screenshotFolder;
        String size = FormatStr.INSTANCE.byteSizeToString(this,
                StorageManager.INSTANCE.getDirectoryMemoryUsage(directory));
        new AlertDialog.Builder(this)
                .setTitle(R.string.dialog_title_result_upload)
                .setMessage(String.format(getString(R.string.dialog_text_result_upload), size))
                .setNegativeButton(R.string.dialog_btn_upload_with, (DialogInterface dialog, int which) ->
                        prepareBenchmarkUpload(true, data)
                )
                .setNeutralButton(R.string.dialog_btn_upload_without, (DialogInterface dialog, int which) ->
                        prepareBenchmarkUpload(false, data)
                )
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mGoogleConnectionHandler = GoogleConnectionHandler.getInstance();
        mGoogleConnectionHandler.setGoogleSignInClient(this, this);
        if (requestCode == Constants.RequestCodes.OPENGL) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.dialog_title_sample_deletion)
                    .setMessage(R.string.dialog_text_sample_free_space)
                    .setNegativeButton(R.string.dialog_btn_yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            StorageManager.INSTANCE.deleteDirectory(StorageManager.INSTANCE.getDirectory() + StorageManager.mediaFolder);
                            startUploadDialog(data);
                        }
                    })
                    .setNeutralButton(R.string.dialog_btn_no, (DialogInterface dialog, int which) ->
                            startUploadDialog(data)
                    ).show();
        } else if (requestCode == Constants.RequestCodes.GOOGLE_CONNECTION) {
            /* Starts the BenchGLActivity to get gpu information */
            if (mGoogleConnectionHandler.handleSignInResult(data)) {
                startActivityForResult(new Intent(ResultPage.this, BenchGLActivity.class),
                        Constants.RequestCodes.OPENGL);
            } else {
                Log.e(TAG, "onActivityResult: failed to log in google");
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoogleConnectionHandler = GoogleConnectionHandler.getInstance();
        mGoogleConnectionHandler.setGoogleSignInClient(this, this);
        if (!hasSendData) {
            hasSendData = true;
            mGoogleConnectionHandler.signIn();
            if (mGoogleConnectionHandler.isConnected()) {
                startActivityForResult(new Intent(ResultPage.this, BenchGLActivity.class),
                        Constants.RequestCodes.OPENGL);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mGoogleConnectionHandler.unsetGoogleSignInClient();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (Util.isAndroidTV(this) &&
                mRecyclerView != null &&
                mRecyclerView.hasFocus() &&
                mRecyclerView.findViewHolderForLayoutPosition(0) != null &&
                !mRecyclerView.findViewHolderForLayoutPosition(0).itemView.hasFocus()) {
            mRecyclerView.findViewHolderForLayoutPosition(0).itemView.requestFocus();
            return ;
        }
        super.onBackPressed();
    }

    class ResultListAdapter extends RecyclerView.Adapter<ResultListAdapter.ViewHolder> {
        ArrayList<ResultModel> mData;

        class ViewHolder extends RecyclerView.ViewHolder {
            private TextView title;
            private TextView subtitle;
            private TextView score;

            ViewHolder(View view) {
                super(view);

                title = view.findViewById(R.id.test_name);
                subtitle = view.findViewById(R.id.test_type);
                score = view.findViewById(R.id.test_score);

                view.setOnClickListener(v -> {
                    ActionBar actionBar = getSupportActionBar();
                    if (actionBar == null || actionBar.getTitle() == null) {
                        Log.e(TAG, "onClickMethod: Failed to get action bar title");
                        return;
                    }
                    Bundle arguments = new Bundle();
                    arguments.putParcelable("result", mData.get(getAdapterPosition()));
                    ResultDetailDialog dialog = new ResultDetailDialog();
                    dialog.setArguments(arguments);
                    dialog.show(getSupportFragmentManager(), "Result Dialog");
                });
            }

            void setTitle(int position) {
                this.title.setText(mData.get(position).getName());
            }

            void setSubtitle(int position) { this.subtitle.setText(mData.get(position).getPrettyTypeString()); }

            void setResult(int position) {
                this.score.setText(
                        (FormatStr.INSTANCE.format2Dec(mData.get(position).getScore()) +
                                " / " + FormatStr.INSTANCE.format2Dec(mData.get(position).getMaxScore())));
            }
        }

        ResultListAdapter(ArrayList<ResultModel> data) {
            mData = data;
        }

        @Override
        public ResultListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_sample_rows, parent, false);
            return new ResultListAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.setTitle(position);
            holder.setResult(position);
            holder.setSubtitle(position);
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }
    }
}
