package org.videolan.vlcbenchmark.tests

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TestSample(
        var url: String = "",
        var name: String = "",
        var checksum: String = "",
        var localUrl: String = "",
        var size: Int = 0
) :  Parcelable