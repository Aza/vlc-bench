package org.videolan.vlcbenchmark.tests

import android.content.Context
import android.os.Parcelable
import android.util.Log
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue
import org.videolan.vlcbenchmark.R
import org.videolan.vlcbenchmark.tools.FormatStr

@Parcelize
class TestController(
        private var testList: @RawValue ArrayList<Test>,
        var currentIndex: Int = 0
) : Parcelable {

    fun next() : Boolean {
        Log.e("TestController", "next: ")
        if (currentIndex + 1 < testList.size) {
            currentIndex += 1
            return true
        }
        return false
    }

    fun setCurrentTestIndex(index: Int) {
        this.currentIndex = index
    }

    fun reset() {
        currentIndex = 0
    }

    fun getCurrentTest() : Test {
        return testList[currentIndex]
    }

    fun getTestNumber() : Int {
        return testList.size
    }

    fun getProgress(loopTotal: Int) : Double {
        return (currentIndex.toDouble() * loopTotal.toDouble()) /
                (testList.size.toDouble() * loopTotal) * 100
    }

    fun getProgressString(context: Context, loopIndex: Int, loopTotal: Int) : String {
        return if (loopTotal > 1) {
            String.format(
                    context.getString(R.string.progress_text_format_loop),
                    FormatStr.format2Dec(getProgress(loopTotal)), currentIndex + 1,
                    testList.size, testList[currentIndex].getPrettyTypeString(),
                    loopIndex, loopTotal
            )
        } else {
            String.format(
                    context.getString(R.string.progress_text_format),
                    FormatStr.format2Dec(getProgress(loopTotal)), currentIndex + 1,
                    testList.size, testList[currentIndex].getPrettyTypeString()
            )
        }
    }

    fun getSampleList() : ArrayList<TestSample> {
        val sampleList = ArrayList<TestSample>()
        for (test in testList) {
            if (test.sample !in sampleList)
                sampleList.add(test.sample)
        }
        return sampleList
    }

}