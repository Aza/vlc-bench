package org.videolan.vlcbenchmark.tests

import android.content.Context
import android.content.Intent
import android.os.Parcelable
import org.videolan.vlcbenchmark.VLCWorkerModel

interface ITest : Parcelable {
    fun prepareIntent(context: Context, intent: Intent, listener: VLCWorkerModel.OnIntentCreatedListener)
    fun getPrettyTypeString(): String
    fun getTypeString() : String
}