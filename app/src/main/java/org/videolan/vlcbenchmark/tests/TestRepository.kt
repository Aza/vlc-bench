package org.videolan.vlcbenchmark.tests

import android.content.Context
import android.util.Log
import org.json.JSONArray
import org.json.JSONObject
import org.videolan.vlcbenchmark.Constants
import org.videolan.vlcbenchmark.R
import org.videolan.vlcbenchmark.tests.types.TestPlayback
import org.videolan.vlcbenchmark.tests.types.TestQuality
import org.videolan.vlcbenchmark.tests.types.TestSpeed
import org.videolan.vlcbenchmark.tools.StorageManager
import org.videolan.vlcbenchmark.tools.StorageManager.delete
import org.videolan.vlcbenchmark.tools.StorageManager.getInternalDirStr
import java.io.BufferedReader
import java.io.File
import java.io.IOException
import java.net.URL

object TestRepository {
    @Suppress("UNUSED")
    private val TAG = this::class.java.name

    private fun getJsonArrayContent(jsonArray: JSONArray, testType: Constants.TestType): ArrayList<Test> {
        val testList = ArrayList<Test>()
        for (i in 0 until jsonArray.length()) {
            val testJson = JSONObject(jsonArray[i].toString())
            val test = when(testType) {
                Constants.TestType.QUALITY -> TestQuality(testJson)
                Constants.TestType.PLAYBACK -> TestPlayback(testJson)
                Constants.TestType.SPEED -> TestSpeed(testJson)
                else -> {
                    return ArrayList()
                }
            }
            testList.add(test)
        }
        return testList
    }

    private fun getTestListFromJson(jsonConfigStr: String) : ArrayList<Test> {
        Log.d(TAG, "getTestListFromJson: start")
        val testList = ArrayList<Test>()
        val jsonObject = JSONObject(jsonConfigStr)

        if (jsonObject.has("quality")) {
            testList.addAll(getJsonArrayContent(jsonObject.getJSONArray("quality"), Constants.TestType.QUALITY))
        }
        if (jsonObject.has("playback")) {
            testList.addAll(getJsonArrayContent(jsonObject.getJSONArray("playback"), Constants.TestType.PLAYBACK))
        }
        if (jsonObject.has("speed")) {
            testList.addAll(getJsonArrayContent(jsonObject.getJSONArray("speed"), Constants.TestType.SPEED))
        }

        testList.sortWith(Comparator { o1, o2 ->
            val n1 = o1!!.sample.name.split("_")[0].toInt()
            val n2 = o2!!.sample.name.split("_")[0].toInt()
            n1.compareTo(n2)
        })
        Log.d(TAG, "getTestListFromJson: end")
        return testList
    }

    @Throws(IOException::class)
    fun getTestController(context: Context): TestController {
        return TestController(getTestListFromJson(requestConfigFile(context)))
    }

    @Throws(IOException::class)
    private fun requestConfigFile(context: Context): String {
        val url = URL(context.getString(R.string.config_file_location_url))
        val connection = url.openConnection()
        val input = connection.getInputStream()
        val reader = BufferedReader(input.reader())
        return reader.readText()
    }

    fun deleteFiles(): Boolean {
        val dirpath = getInternalDirStr(StorageManager.jsonFolder)
        if (dirpath == null) {
            Log.e(TAG, "Failed to get folder path")
            return false
        }
        val dir = File(dirpath)
        val files = dir.listFiles()
        for (file in files) {
            delete(file!!)
        }
        return true
    }
}