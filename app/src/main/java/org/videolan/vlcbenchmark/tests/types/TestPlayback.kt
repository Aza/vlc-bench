package org.videolan.vlcbenchmark.tests.types

import android.content.Context
import android.content.Intent
import kotlinx.android.parcel.Parcelize
import org.json.JSONObject
import org.videolan.vlcbenchmark.Constants
import org.videolan.vlcbenchmark.VLCWorkerModel
import org.videolan.vlcbenchmark.tests.Test
import org.videolan.vlcbenchmark.tests.TestSample
import org.videolan.vlcbenchmark.tools.StorageManager

@Parcelize
class TestPlayback(
        override var sample: TestSample = TestSample(),
        override var type: Constants.TestType = Constants.TestType.PLAYBACK,
        override var hardware: Boolean = false
) : Test(sample, type, hardware) {
    constructor(jsonObject: JSONObject) : this() {
        this.sample.name = jsonObject.getString("name")
        this.hardware = jsonObject.getBoolean("hardware")
        this.sample.url = jsonObject.getString("url")
        this.sample.checksum = jsonObject.getString("checksum")
        this.sample.size = jsonObject.getInt("size")
        this.sample.localUrl = StorageManager.getInternalDirStr(StorageManager.mediaFolder)!! + "/" + this.sample.name
    }

    override fun prepareIntent(context: Context, intent: Intent, listener: VLCWorkerModel.OnIntentCreatedListener) {
        intent.putExtra(Constants.Extras.ACTION, Constants.Extras.ACTION_PLAYBACK)
        super.prepareIntent(context, intent, listener)
    }
}